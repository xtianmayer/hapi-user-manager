'use strict';
const UserStore = require('./data/userstore'),
	_ = require('lodash');

let Handlers = {};

Handlers.indexHandler = (request, reply) => {
	reply.view('index');
}

Handlers.usersHandler = (request, reply) => {
	reply.view('users', { users: UserStore.users });
}

Handlers.userHandler = (request, reply) => {
	const user = _.findWhere(UserStore.users, { id: request.params.userid });
	const isEdit = (request.query.edit && request.query.edit == 'true')
	
	reply.view((isEdit ? 'user-edit' : 'user-detail'), { user: user });
}

Handlers.deleteUserHandler = (request, reply) => {
	_.remove(UserStore.users, (user) => {
		return user.id == request.params.userid;
	});
	reply();
}

Handlers.updateUserHandler = (request, reply) => {
	const user = _.findWhere(UserStore.users, { id: request.params.userid });
	_.merge(user,request.payload)
	reply();
}


Handlers.assetHandler = {
	directory: {
		path: 'assets',
		/*listing: true*/
	}
}

module.exports = Handlers;