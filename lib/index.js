'use strict';

const 	Hapi = require('hapi'),
		UserStore = require('./data/userstore');

let server = new Hapi.Server();

server.connection({ port: process.env.PORT || 3000, routes: {
        validate: {
            options: {
                abortEarly: false
            }
        }
    } });

server.ext('onPreResponse', (request, reply)=> {

    var response = request.response;
    if (response.isBoom) {
		console.log(response.output);
        return reply.view('error', response.output);
    }

    return reply.continue();
});

UserStore.initialize();

server.register([require('vision'), require('inert')], (err) => {
	if (err) {
		throw err;
	}
	
	const handlebars = require('handlebars');
	server.views({
		engines: {
			html: handlebars
		},
		relativeTo: __dirname,
		path: '../views',
		partialsPath: '../views/partials',
		layoutPath: '../views/layout',
		helpersPath: '../views/helpers',
		layout: true,
		isCached: false
	});
	
	server.route(require('./routes'))

});

server.start((err) => {
	if (err) {
		throw err;
	}
	console.log('Server is up: ', server.info.uri);

});
