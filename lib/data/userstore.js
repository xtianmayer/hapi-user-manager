'use strict';

let UserStore = {},
	Fs = require('fs'),
	Path = require('path');

UserStore.users = {};

UserStore.initialize = () => {
	UserStore.users = loadUsers();
};

function loadUsers() {
	const file = Fs.readFileSync(Path.join(__dirname, './users.json'));
	return JSON.parse(file.toString());
};

module.exports = UserStore;