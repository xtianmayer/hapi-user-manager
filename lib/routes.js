'use strict';

const Handlers = require('./handlers'),
	Joi = require('joi'),
	Boom = require('boom');

module.exports = [
	{
		path: '/',
		method: 'GET',
		handler: Handlers.indexHandler
	},
	{
		path: '/users',
		method: 'GET',
		handler: Handlers.usersHandler
	},
	{
		path: '/user/{userid}',
		method: 'GET',
		handler: Handlers.userHandler
	},
	{
		path: '/user/{userid}',
		method: 'DELETE',
		handler: Handlers.deleteUserHandler
	},
	{
		path: '/user/{userid}',
		method: 'PUT',
		config: {
			validate: {
				payload: {
					id: Joi.string().required(),
					name: Joi.string().required().max(100),
					company: Joi.string().required().max(100),
					email: Joi.string().email().required()
				}
			},
			handler: Handlers.updateUserHandler
		}
	},
	{
		path: '/badroute',
		method: 'GET',
		handler: (request, reply) => {
			reply(Boom.notImplemented('Route has not been implemented'));
		}
	},
	{
		path: '/{param*}',
		method: 'GET',
		handler: Handlers.assetHandler
	}];