# Simple User Manager Application with Hapi

Basic application to show how to start with Hapijs. Concepts covered are:

* Hapi Server setup
* Creating Routes and Route Handlers
* Templating and Template Helpers
* Authentication
* Database integration using MySQL, Postgres or Mongo

## Requirements

Make sure to have the following components setup on your machine:

* npm

##### TSD
TypeScript Definitions are not required, however, make working with Javascript Frameworks easier. 
> npm install tsd -g