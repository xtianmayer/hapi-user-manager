/**
 * Single exports to handle manipulation of context
*/
module.exports = (context) => context.toUpperCase();